var Core = require('./core/core');
    
var core = new Core(true);
//core.addModule('../helloDebug', {}, 'debug');
//core.addModule('debug');
core.addModule('pong');
core.addModule('ping');


console.log("Initializing core...");
core.init()
    .then(()=>{
        console.log("Core initialized!");
        return core.start(__dirname + '/test');
    })
    .catch(err => {
        console.log("Error while initializing core...", err);
    });
