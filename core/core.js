'use strict';

var Promise = require('bluebird');
var _ = require('lodash');
var assert = require('assert');

var Core = function(useSharedModules){
    this.moduleDescriptions = [];
    this.sharedModules = {};
    this.useSharedModules = useSharedModules;
};


Core.prototype.addModule = function(moduleName, config, alias){
    this.moduleDescriptions.push({
        moduleName: moduleName,
        config: config,
        alias: alias
    });
};


Core.prototype.init = function(){
    var core = this;

    console.log("Injecting module-sharing to common-js' require function...");

    if(core.useSharedModules){
        // instead of this hook we could use
        // System.import('moduleName').then(module => {
        //   System.set('someModuleName', module)
        // });
        // if es6 moduleloader is integrated in node and
        // it does not support sharing modules 
        module.constructor.prototype.require = function (path) {
            var self = this;

            assert(path, 'missing path');
            assert(typeof path === 'string', 'path must be a string');
            
            var sharedModule = core.sharedModules[path];
            if(sharedModule){
                console.log("Using shared-module for '" + path + "' from ", self.id);
                return sharedModule;
            }
            
            return self.constructor._load(path, self);
        }
    }

    console.log("Loading all added modules...")
    var p = Promise.resolve();
    
    core.moduleDescriptions.forEach(function(m){
        p = p.then(loadModule(core.sharedModules, m));
    });
    
    return p;
};

Core.prototype.start = function(moduleName){
    require(moduleName).exec();
}


function loadModule(modulesMap, m){
    return function(){
        console.log('Loading module ' + m.moduleName);
        var module = require(m.moduleName);
        console.log('Module ' + m.moduleName + ' loaded.');
        registerModule(modulesMap, module, module.name);
        registerModule(modulesMap, module, m.alias);
        console.log("Shared modules are now: ", _.keys(modulesMap));

    };
}


function registerModule(modulesMap, module, name){
    if(!module || !name){return null;}
    console.log('Registering as system module ' + name);
    assert(!modulesMap[name], "Module '" + name + "' already registed...");
    return modulesMap[name] = module;
}


module.exports = Core;
