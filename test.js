'use strict';

var ping = require('ping').ping;
var pong = require('pong').pong;

module.exports.name = 'test';

module.exports.exec = function(){
    pong();
    ping();
}
